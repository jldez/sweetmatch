from sweetmatch import match

x = [1, 2, 3]
y = [1.6, 2.1, 3.2]

def cost_function(x:int, y:float) -> float:
    return abs(x - y)

matches = match(x, y, cost_function)

print(matches)

# [(1, 1.6), (2, 2.1), (3, 3.2)]


matches = match(x, y, cost_function, cost_threshold=0.5)

print(matches)

# [(2, 2.1), (3, 3.2)]